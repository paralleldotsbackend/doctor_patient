import os,sys
import json
import time
import calendar
import datetime
from tornado import ioloop,web
import urllib
import redis,ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
import os, uuid
import numpy
from pymongo import MongoClient
from bson import ObjectId

patient_data = MongoClient("104.155.210.134")["patient"]["patient_doc"]

__UPLOADS__ = "static/uploads/"

class PicsUploadHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        fileinfo        = self.request.files['file1'][0]
        dob             = self.get_body_argument("dob")
        gender          = self.get_body_argument("gender")
        ethnicity       = self.get_body_argument("ethnicity")
        diabetes        = self.get_body_argument("diabetes")
        smoking         = self.get_body_argument("smoking")
        other_info      = self.get_body_argument("other_info")
        doctor_id       = self.get_body_argument("doc_id") 
        fname           = fileinfo['filename']
        fname1          = fname                
        extn            = os.path.splitext(fname1)[1]
        check_for_fname = patient_data.find_one({'doctor_id':doctor_id, 'local_image_name':fname1})
        
        if check_for_fname is not None:
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':0,'msg':"Image with this name already uploaded"}))
            return

        fname     = fname.split(".")
        fname     = fname[0]
        now       = datetime.date.today()
        startdate = now.strftime("%d%B%Y")
        cname     = str(fname)+"_tagged_"+str(startdate)+"_bwxKG"+str(extn)
        
        fh    = open(__UPLOADS__ + cname, 'w')
        fh.write(fileinfo['body'])
        insert = patient_data.insert({'created_at':datetime.datetime.now(),'local_image_name':fname1,'dob':dob,'gender':gender,'ethnicity':ethnicity,'diabetes':diabetes,'smoking':smoking,'other_info':other_info,'image':cname,'doctor_id':doctor_id,'save_as_draft':0,'mask':{}})
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'pid':str(insert)}))
        return

class ServeImagesHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        doc_id = self.get_argument("doc_id")
        data   = patient_data.find({'doctor_id':doc_id,'save_as_draft':0})
        drafts = []
        for obj in data:
            drafts.append({'image_name':str(obj.get('local_image_name')),'created_at':str(obj.get('created_at')),'pid':str(obj.get('_id')),'mask':obj.get('mask'),'image_link':"http://utilities.paralleldots.com/doctor/"+str(obj.get('image'))})
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'drafts':drafts}))
        return

class UpdatePatientHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        p_data      = json.loads(urllib.unquote_plus(self.request.body)) 
        pid         = p_data.get('pid',None)
        dob         = p_data.get('dob',None)
        gender      = p_data.get('gender',None)
        ethnicity   = p_data.get('ethnicity',None)
        diabetes    = p_data.get('diabetes',None)
        smoking     = p_data.get('smoking',None)
        other_info  = p_data.get('other_info',None)
        smoking     = p_data.get('smoking',None)
        doctor_id   = p_data.get('doc_id',None)
        
        update      = patient_data.update({'_id': ObjectId(str(pid))}, {'$set': {'dob':dob,'gender':gender,'ethnicity':ethnicity,'diabetes':diabetes,'smoking':smoking,'other_info':other_info,'doctor_id':doctor_id} })
        
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1}))
        return


class SaveDraftHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        mask_data  = json.loads(urllib.unquote_plus(self.request.body)) 
        pid        = mask_data.get('pid',None)
        mask       = mask_data.get('mask',None)
        update     = patient_data.update({'_id': ObjectId(str(pid))}, {'$set': {'mask':json.dumps(json.loads(mask))} })
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1}))
        return


settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : False
}

application = web.Application([
    (r'/patient/api/images/uploads', PicsUploadHandler),
    (r'/patient/api/serve/images', ServeImagesHandler),
    (r'/patient/api/save/draft', SaveDraftHandler),
    (r'/patient/api/update', UpdatePatientHandler),
],**settings)

if __name__ == "__main__":
    print "Here we go"
    application.listen(8895)
    ioloop.IOLoop.instance().start()
